-- phpMyAdmin SQL Dump
-- version 2.11.5
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 建立日期: May 28, 2008, 12:26 AM
-- 伺服器版本: 5.0.51
-- PHP 版本: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- 資料庫: `thesis`
--

-- --------------------------------------------------------

--
-- 資料表格式： `cause`
--

CREATE TABLE `cause` (
  `fromID` int(10) NOT NULL,
  `toID` int(10) NOT NULL,
  PRIMARY KEY  (`fromID`,`toID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表格式： `codes`
--

CREATE TABLE `codes` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `component` char(6) collate utf8_unicode_ci NOT NULL,
  `year` smallint(4) unsigned zerofill NOT NULL,
  `receiver` varchar(16) collate utf8_unicode_ci NOT NULL,
  `openCoding` tinytext collate utf8_unicode_ci NOT NULL,
  `axialCodingForComponent1` varchar(255) collate utf8_unicode_ci NOT NULL,
  `axialCodingForComponent2` varchar(255) collate utf8_unicode_ci NOT NULL,
  `axialCodingForComponent3` varchar(255) collate utf8_unicode_ci NOT NULL,
  `axialCodingForComponent4` varchar(255) collate utf8_unicode_ci NOT NULL,
  `axialCodingForReceiver` varchar(255) collate utf8_unicode_ci NOT NULL,
  `source` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1091 ;
