<?php
require_once 'config.php';

$mainStartYear = isset($_GET['msy']) ? $_GET['msy'] : 2004;
$mainEndYear = isset($_GET['mey']) ? $_GET['mey'] : 2008;

$compareStartYear = isset($_GET['csy']) ? $_GET['csy'] : 2004;
$compareEndYear = isset($_GET['cey']) ? $_GET['cey'] : 2006;

$unit = isset($_GET['unit']) ? $_GET['unit'] : 3;
$level = isset($_GET['level']) ? $_GET['level'] : 2;

if(!($mainStartYear && $mainEndYear && $compareStartYear && $compareEndYear && $unit && $level))
{
    exit;
}

function drawLine($im, $fromNode, $toNode, $degree, $radius1, $radius2, $lineColor)
{
    global $allElementsXY;
    
    imageline($im,
              $allElementsXY[$fromNode]['x'], $allElementsXY[$fromNode]['y'],      
              $allElementsXY[$toNode]['x'],$allElementsXY[$toNode]['y'],
              $lineColor);
        
    if(false)
    {
        imagettftext($im, 12, 0, 
                     ($allElementsXY[$fromNode]['x']+$allElementsXY[$toNode]['x'])/2, 
                     ($allElementsXY[$fromNode]['y']+$allElementsXY[$toNode]['y'])/2,
                     $fontColor, "fonts\arial.ttf",  $degree);          
    }
        
    $minusX = $allElementsXY[$toNode]['x'] - $allElementsXY[$fromNode]['x'];
    $minusY = $allElementsXY[$toNode]['y'] - $allElementsXY[$fromNode]['y'];

    if($minusX == 0)
    {
        if($minusY > 0)
        {
            $plus = M_PI_2;
        }
        else
        {
            $plus = M_PI_2 * 3;
        }
    } 
    elseif($minusY == 0)
    {
        if($minusX > 0)
        {
            $plus = 0;
        }
        else
        {
            $plus = M_PI;
        }
    }
    else
    {
        $plus = atan2($minusY,$minusX);
    }
    $minusW = ($radius1 + $radius2 )* cos($plus);
    $minusH = ($radius1 + $radius2 ) * sin($plus);            
     
       
        
    $value = array(
      0  => $allElementsXY[$toNode]['x'] - $minusW  + $radius2 * cos(deg2rad(0)+ $plus),    // x1
      1  => $allElementsXY[$toNode]['y'] - $minusH   + $radius2 * sin(deg2rad(0) + $plus),    // y1
      2  => $allElementsXY[$toNode]['x'] - $minusW   + $radius2 * cos(deg2rad(120) + $plus),    // x2
      3  => $allElementsXY[$toNode]['y'] - $minusH  + $radius2 * sin(deg2rad(120) + $plus),   // y2
      4  => $allElementsXY[$toNode]['x'] - $minusW  + $radius2 * cos(deg2rad(240) + $plus),    // x3
      5  => $allElementsXY[$toNode]['y'] - $minusH  + $radius2 * sin(deg2rad(240) + $plus),    // y3
    );
    imagefilledpolygon($im, $value, 3, $lineColor);
}

$mainElements = findCoreElements($mainStartYear, $mainEndYear, $unit, $level);
$mainRelations = findRelations($mainStartYear, $mainEndYear, $unit, $level, $mainElements);

$compareElements = findCoreElements($compareStartYear, $compareEndYear, $unit, $level);
$compareRelations = findRelations($compareStartYear, $compareEndYear, $unit, $level, $compareElements);

$allElements = array_merge($mainElements, array_diff($compareElements, $mainElements)  );
natsort($allElements);

if(count($allElements) <= 10)
{
    $imageWidth = 250;
    $imageHeight = 250;
    $margin = 30;
}
elseif(count($allElements) <= 20)
{
    $imageWidth = 300;
    $imageHeight = 300;
    $margin = 30;
}
else
{
    $imageWidth = 350;
    $imageHeight = 350;
    $margin = 30;
}



$im = @imagecreatetruecolor(($imageWidth+$margin) * 2 + 150, ($imageHeight+$margin)* 2 )  or die("無法建立圖片！");
        
$bgColor = imagecolorallocate($im, 240,240,240);

$deleteNodeColor = imagecolorallocate($im, 200,200,200);
$consistNodeColor = imagecolorallocate($im, 0,0,100);
$newNodeColor = imagecolorallocate($im, 150,0,0);

$deleteLineColor = imagecolorallocate($im, 200,200,200);
$consistLineColor = imagecolorallocate($im, 0,0,150);
$newLineColor = imagecolorallocate($im, 200,0,0);

imagefill($im,0,0,$bgColor);


$radius1 = 7; // 圓半徑
$radius2 = 5;  // 箭頭半徑

$allElementsXY = array();

for($i=0 ; $i<count($allElements) ; $i++ )
{
    $allElementsXY[$allElements[$i]]['x'] = $margin + $imageWidth * (1 + cos(deg2rad(360 * $i / count($allElements))));
    $allElementsXY[$allElements[$i]]['y'] = $margin + $imageHeight * (1 + sin(deg2rad(360 * $i / count($allElements)))); 
}

foreach( $mainRelations as $fromNode => $toSets)
{
    foreach($toSets as $toNode => $degree)
    {
        if(isset($compareRelations[$fromNode][$toNode]))
        {
            drawLine($im, $fromNode, $toNode, $degree, $radius1, $radius2, $consistLineColor);
        }
        else
        {
            drawLine($im, $fromNode, $toNode, $degree, $radius1, $radius2, $newLineColor);
        }
        
    }
}
       
foreach( $compareRelations as $fromNode => $toSets)
{
    foreach($toSets as $toNode => $degree)
    {
        if(!isset($mainRelations[$fromNode][$toNode]))
        {
            drawLine($im, $fromNode, $toNode, $degree, $radius1, $radius2, $deleteLineColor);
        }        
    }
}

foreach($allElementsXY as $node => $XY)
{
    if(in_array($node, $mainElements) && in_array($node, $compareElements))
    {
        $nodeColor = $consistNodeColor;
        $lineColor = $consistLineColor;
    }
    elseif(!in_array($node, $mainElements) && in_array($node, $compareElements))
    {
        $nodeColor = $deleteNodeColor;
        $lineColor = $deleteLineColor;
    }
    elseif(in_array($node, $mainElements) && !in_array($node, $compareElements))
    {
        $nodeColor = $newNodeColor;
        $lineColor = $newLineColor;
    }

    imagefilledellipse($im, $XY['x'], $XY['y'], $radius1 * 2, $radius1 * 2, $nodeColor);
    imageellipse($im, $XY['x'], $XY['y'], $radius1 * 2, $radius1 * 2, $lineColor);
    imagettftext($im, 10, 0, $XY['x']+$radius1, $XY['y']+$radius1*2, $nodeColor, "fonts\mingliu.ttc",  $node);          
}


header("Content-type:image/gif");
imagepng($im);
imagedestroy($im);


mysql_close($link);
?>
