<?php

function causeQuery($startYear, $endYear, $unit=3, $level=2)
{

    switch($unit)
    {
        case 1: // 以事件為單位
return '    
SELECT fCode.year as fromYear, fCode.component as fromComponent ,fCode.axialCodingForComponent1 as fromAxialCoding,
       tCode.year as toYear, tCode.component as toComponent,tCode.axialCodingForComponent1 as toAxialCoding
FROM cause
LEFT JOIN codes AS fCode ON fromID = fCode.id
LEFT JOIN codes AS TCode ON toID = tCode.id
WHERE tCode.year <= '.$endYear.' AND tCode.year >= '.$startYear;
          break;

      case 2:  // 以年為單位

return '
SELECT fCode.year as fromYear, fCode.component as fromComponent ,fCode.axialCodingForComponent1 as fromAxialCoding,
       tCode.year as toYear, tCode.component as toComponent,tCode.axialCodingForComponent1 as toAxialCoding
FROM cause
LEFT JOIN codes AS fCode ON fromID = fCode.id
LEFT JOIN codes AS TCode ON toID = tCode.id
WHERE tCode.year <= '.$endYear.' AND tCode.year >= '.$startYear.'
GROUP BY fCode.year, fCode.component, fCode.axialCodingForComponent1,
         tCode.year, tCode.component, tCode.axialCodingForComponent1
ORDER BY fCode.axialCodingForComponent1';
          break;

    case 3: // 以因果為單位(最早發生)
return 'SELECT fCode.year as fromYear, fCode.component as fromComponent ,fCode.axialCodingForComponent1 as fromAxialCoding,
       tCode.year as toYear, tCode.component as toComponent,tCode.axialCodingForComponent1 as toAxialCoding,
       count(*)
FROM cause AS c1
LEFT JOIN codes AS fCode ON c1.fromID = fCode.id
LEFT JOIN codes AS tCode ON c1.toID = tCode.id
WHERE tCode.year <= '.$endYear.' AND tCode.year >= '.$startYear.'
  AND NOT EXISTS(SELECT * FROM cause AS c2
                          LEFT JOIN codes AS fCode2 ON c2.fromID = fCode2.id
                          LEFT JOIN codes AS tCode2 ON c2.toID = tCode2.id 
                         WHERE tCode.year > tCode2.year
                           AND fCode.year > fCode2.year'.
                           ($level == 1 ? '
                           AND fCode.component = fCode2.component
                           AND tCode.component = tCode2.component'
                           : '
                           AND fCode.component = fCode2.component 
                           AND fCode.axialCodingForComponent1 = fCode2.axialCodingForComponent1
                           AND tCode.component = tCode2.component 
                           AND tCode.axialCodingForComponent1 = tCode2.axialCodingForComponent1'
                           ).'
      )
GROUP BY '.
    ($level == 1 ? '
    fCode.component,
    tCode.component'
    : '
    fCode.component, fCode.axialCodingForComponent1,
    tCode.component, tCode.axialCodingForComponent1').'
ORDER BY fCode.year ASC, fCode.component ASC, tCode.year ASC';
        break;
    }
}

function subNodeNum($layer, $set)
{
    if($layer == 0 )
    {
        return 1;
    }
    elseif($layer == 1)
    {
        return 1;
    }
    else
    {
        $num = 0;
        foreach($set as $key => $subSet)
        {
            $num += subNodeNum($layer-1,$subSet);
        }
        return $num;
    }
}

function Tree2Table($layer,$dept,$tree, $isColor=false)
{
    $color[1] = '#00F0F0';
    $color[2] = '#00D0D0';
    $color[3] = '#00B0B0';
    $color[4] = '#009090';
    $color[5] = '#007070';
    $color[6] = '#005050';
    $color[7] = '#003030';
    $color[8] = '#001010';
    $color[9] = '#001010';
    $color[10] = '#001010';
    $color[11] = '#001010';
    $color[12] = '#001010';
    $color[13] = '#001010';

    if($layer > $dept)
    {
        return false;
    }
    if($layer == $dept)
    {
        if(is_array($tree))
        {
            foreach($tree as $key => $node)
            {
                if($node == '')
                {
                    if($isColor)
                    {
                        echo '<td>&nbsp;</td>';
                    }
                    else
                    {
                        echo '<td>&nbsp;</td>';
                    }

                }
                else
                {
                    if($isColor)
                    {
                        if($node>count($color)) $node=count($color);
                        echo '<td align="center" bgcolor="'.$color[$node].'">'.$node.'</td>';
                    }
                    else
                    {
                        echo '<td>'.$node.'</td>';
                    }

                }
            }
        }
        else
        {
            if($isColor)
            {
                echo '<td>'.$tree.'</td>';
            }
            else
            {
                echo '<td>'.$tree.'</td>';
            }

        }
        echo '</tr>';
    }
    else
    {
        $i=1;
        foreach($tree as $node => $subTree)
        {
            if($i != 1)
            {
                echo '<tr>';
            }

            if($isColor)
            {
                echo '<td bgcolor="#F0FFFF" rowspan="'.subNodeNum($dept-$layer,$subTree).'">'.$node.'</td>';
            }
            else
            {
                echo '<td rowspan="'.subNodeNum($dept-$layer,$subTree).'">'.$node.'</td>';
            }


            Tree2Table($layer+1,$dept,$subTree,$isColor);
            $i++;
        }
    }
}


function iterative($fCode, $beta, $iterative, $level=0)
{
    global $degree;
    global $cause;

    if($iterative == 0)
    {
        return $degree[$fCode] * pow($beta,$level);
    }

    $sum = $degree[$fCode] * pow($beta,$level);
    foreach($cause[$fCode] as $tCode => $isConnected)
    {
        if($isConnected)
        {
            $sum += iterative($tCode, $beta, $iterative-1, $level+1);
        }
    }
    return $sum;
}

function bonacich_power($startYear, $endYear, $iterative=10, $beta=0.5, $alpha=1,$unit=2, $level=2)
{
    $query = causeQuery($startYear, $endYear, $unit);

    global $component;
    global $cause;
    global $degree;

    $degree = array();
    $cause = array();

    $result = mysql_query($query);

    while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
    {
        switch($level)
        {
            case 1:
                $fCode = '['.$component[$row['fromComponent']].']'.$row['fromComponent'];
                $tCode = '['.$component[$row['toComponent']].']'.$row['toComponent'];
                break;
            case 2:
                $fCode = '['.$component[$row['fromComponent']].']'.$row['fromAxialCoding'];
                $tCode = '['.$component[$row['toComponent']].']'.$row['toAxialCoding'];
                break;
        }

        if($level == 1 && $fCode == $tCode)
        {
            continue;
        }

        if(!array_key_exists($fCode,$cause)) {
            $cause[$fCode] = array();
        }
        $cause[$fCode][$tCode] = 1;

        if(!array_key_exists($fCode,$degree)) {
            $degree[$fCode] = 0;
        }
        if(!array_key_exists($tCode,$degree)) {
            $degree[$tCode] = 0;
        }
        $degree[$fCode]++;
    }

    mysql_free_result($result);

    foreach($degree as $f => $value)
    {
        if(!isset($cause[$f]))
        {
            $cause[$f] = array();
        }

        foreach($degree as $t => $value)
        {
            if(!isset($cause[$f][$t]))
            {
                $cause[$f][$t] = 0;
            }
        }
    }

    $power = array();
    foreach($degree as $code => $value)
    {
        $power[$code] = iterative($code, $beta, $iterative);
    }

    return $power;
}


function path($start, $end, $current='',$queue=array())
{
    global $degree;
    global $cause;

    if($end == $current)
    {
        array_push($queue, $end);
        return $queue;
    }
    elseif(in_array($current,$queue))
    {
        return false;
    }
    else
    {
        if($current == '') $current = $start;

        array_push($queue,$current);

        if(!isset($cause[$current]))
        {
            return false;
        }

        $tree = array();
        foreach($cause[$current] as $tCode => $value)
        {
            $path = path($start, $end, $tCode, $queue);
            if($path)
            {
                $tree = array_merge($tree, $path);
            }
        }

        if($current == $start)
        {
            $tmp = array();
            for($i=0,$j=-1,$k=0 ; $i<count($tree);$i++)
            {
                if($tree[$i] == $start)
                {
                    $j++;
                }

                $tmp[$j][$k++] = $tree[$i];

                if($tree[$i] == $end)
                {
                    $k=0;
                }
            }

            return $tmp;
        }
        else
        {
            return $tree;
        }
    }
}

function betweenness($startYear, $endYear, $unit=2, $level=2)
{

    $query = causeQuery($startYear, $endYear, $unit=2);

    $result = mysql_query($query);

    global $component;
    global $cause;
    global $degree;

    $cause = array();
    $degree = array();

    while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
    {
        switch($level)
        {
            case 1:
                $fCode = '['.$component[$row['fromComponent']].']'.$row['fromComponent'];
                $tCode = '['.$component[$row['toComponent']].']'.$row['toComponent'];
                break;
            case 2:
                $fCode = '['.$component[$row['fromComponent']].']'.$row['fromAxialCoding'];
                $tCode = '['.$component[$row['toComponent']].']'.$row['toAxialCoding'];
                break;
        }

        if(!array_key_exists($fCode,$cause)) {
            $cause[$fCode] = array();
        }
        $cause[$fCode][$tCode] = 1;

        if(!array_key_exists($fCode,$degree)) {
            $degree[$fCode] = 0;
        }
        if(!array_key_exists($tCode,$degree)) {
            $degree[$tCode] = 0;
        }
    }

    mysql_free_result($result);


    foreach($degree as $fCode => $value)
    {
        foreach($degree as $tCode => $value)
        {
            if($fCode != $tCode && !isset($cause[$fCode][$tCode]))
            {
                $paths = path($fCode, $tCode);
                if($paths)
                {
                    $short_paths = array(); // 經過元件
                    $short_path_num = 0;  // 最短路徑數
                    $short_path_node = 0; // 最短路徑元件數
                    foreach($paths as $path)
                    {
                        if($short_path_node == 0 || $short_path_node > count($path))
                        {
                            $short_path_node = count($path);
                            $short_path_num = 1;

                            unset($path[count($path)-1]);
                            unset($path[0]);
                            $short_paths = $path;
                        }
                        elseif($short_path_node == count($path))
                        {
                            $short_path_num++;
                            unset($path[count($path)-1]);
                            unset($path[0]);
                            $short_paths = array_merge($short_paths, $path);
                        }
                    }

                    foreach($short_paths as $node)
                    {
                        $degree[$node] += 1/$short_path_num;
                    }
                }

            }
        }
    }
    return $degree;
}

function normalize($data)
{
    if(count($data) <= 1)
    {
        return array();
    }

    $sum = array_sum($data);
    $num = count($data);
    $mean = $sum / $num;

    $tmp = 0;
    foreach($data as $value)
    {
        $tmp += pow($value,2) - $value * $mean;
    }
    $s = sqrt($tmp / ($num -1));

    if(!$s)
    {
        return array();
    }

    $z = array();
    foreach($data as $key => $value)
    {
        $z[$key] = ($value - $mean) / $s;
    }

    return $z;
}

function degree($startYear, $endYear, $unit=2,$level=2)
{
    global $component;

    $indegree = array();
    $outdegree = array();
    $alldegree = array();

    $query = causeQuery($startYear, $endYear, $unit);
    $result = mysql_query($query);

    while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
    {
        switch($level)
        {
            case 1:
                $fCode = '['.$component[$row['fromComponent']].']'.$row['fromComponent'];
                $tCode = '['.$component[$row['toComponent']].']'.$row['toComponent'];
                break;
            case 2:
                $fCode = '['.$component[$row['fromComponent']].']'.$row['fromAxialCoding'];
                $tCode = '['.$component[$row['toComponent']].']'.$row['toAxialCoding'];
                break;
        }

        if(!array_key_exists($tCode,$indegree)) {
            $indegree[$tCode] = 0;
        }
        if(!array_key_exists($tCode,$outdegree)) {
            $outdegree[$tCode] = 0;
        }
        if(!array_key_exists($fCode,$outdegree)) {
            $outdegree[$fCode] = 0;
        }
        if(!array_key_exists($fCode,$indegree)) {
            $indegree[$fCode] = 0;
        }
        if(!array_key_exists($tCode,$alldegree)) {
            $alldegree[$tCode] = 0;
        }
        if(!array_key_exists($fCode,$alldegree)) {
            $alldegree[$fCode] = 0;
        }

        $indegree[$tCode]++;
        $outdegree[$fCode]++;
        $alldegree[$tCode]++;
        $alldegree[$fCode]++;
    }


    return array('in'=>$indegree, 'out'=>$outdegree, 'all'=>$alldegree);
}


function closeness($startYear, $endYear, $unit=2, $level=2)
{

    $query = causeQuery($startYear, $endYear, $unit=2);

    $result = mysql_query($query);

    global $component;
    global $cause;
    global $degree;

    $cause = array();
    $inFarness = array();
    $outFarness = array();

    while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
    {
        switch($level)
        {
            case 1:
                $fCode = '['.$component[$row['fromComponent']].']'.$row['fromComponent'];
                $tCode = '['.$component[$row['toComponent']].']'.$row['toComponent'];
                break;
            case 2:
                $fCode = '['.$component[$row['fromComponent']].']'.$row['fromAxialCoding'];
                $tCode = '['.$component[$row['toComponent']].']'.$row['toAxialCoding'];
                break;
        }

        if(!array_key_exists($fCode,$cause)) {
            $cause[$fCode] = array();
        }
        $cause[$fCode][$tCode] = 1;

        if(!array_key_exists($fCode,$inFarness)) {
            $inFarness[$fCode] = 0;
            $outFarness[$fCode] = 0;
        }
        if(!array_key_exists($tCode,$inFarness)) {
            $inFarness[$tCode] = 0;
            $outFarness[$tCode] = 0;
        }
    }

    mysql_free_result($result);


    foreach($inFarness as $fCode => $value)
    {
        foreach($inFarness as $tCode => $value)
        {
            if($fCode == $tCode)
            {
                continue;
            }
            else
            {
                $paths = path($fCode, $tCode);
                if($paths)
                {
                    $short_path = array(); // 經過元件
                    $short_path_num = 0;  // 最短路徑數
                    $short_path_node = 0; // 最短路徑元件數
                    foreach($paths as $path)
                    {
                        if($short_path_node == 0 || $short_path_node > count($path))
                        {
                            $short_path_node = count($path);
                            $short_path_num = 1;

//                            unset($path[count($path)-1]);
  //                          unset($path[0]);
                            $short_path = $path;
                        }
                        elseif($short_path_node == count($path))
                        {
                            $short_path_num++;
    //                        unset($path[count($path)-1]);
      //                      unset($path[0]);
                            $short_path = $path;
                        }
                    }

                    $inFarness[$tCode] += count($short_path) - 1;
                    $outFarness[$fCode] += count($short_path) - 1;
                }
                else
                {
                    $inFarness[$tCode] += count($outFarness);
                    $outFarness[$fCode] += count($inFarness);
                }

            }
        }
    }

    $inCloseness = array();
    $outCloseness = array();

    foreach($inFarness as $key => $value)
    {
        if($value)
        {
            $inCloseness[$key] = ((count($outFarness) - 1) * 100) / $value;
        }
        else
        {
            $inCloseness[$key] = 0;
        }
    }

    foreach($outFarness as $key => $value)
    {
        if($value)
        {
            $outCloseness[$key] = ((count($outFarness) - 1) * 100) / $value;
        }
        else
        {
            $outCloseness[$key] = 0;
        }

    }

    return array('inFarness' => $inFarness, 'outFarness' => $outFarness, 'inCloseness' => $inCloseness, 'outCloseness' => $outCloseness);
}


function findCoreElements($startYear, $endYear, $unit, $level)
{
    global $GLOBAL_Z;
    $coreElements = array();

    if($level == 1)
    {
        $iterative = 5;
    }
    elseif($level == 2)
    {
        $iterative = 10;
    }

    $beta = 0.5;
    $alpha = 1;

    $positive_power = bonacich_power($startYear, $endYear,  $iterative, $beta, $alpha, $unit, $level);
    $negative_power = bonacich_power($startYear, $endYear,  $iterative, 0 - $beta, $alpha, $unit, $level);
    $betweenness = betweenness($startYear,$endYear, $unit, $level);
    $degree = degree($startYear, $endYear, $unit, $level);
    $indegree = $degree['in'];
    $outdegree = $degree['out'];
    $alldegree = $degree['all'];
    $closeness = closeness($startYear, $endYear, $unit, $level);
    $inCloseness = $closeness['inCloseness'];
    $outCloseness = $closeness['outCloseness'];


    arsort($positive_power);
    arsort($negative_power);
    arsort($betweenness);
    arsort($indegree);
    arsort($outdegree);
    arsort($alldegree);
    arsort($inCloseness);
    arsort($outCloseness);

    $pz = normalize($positive_power);
    $nz = normalize($negative_power);
    $bz = normalize($betweenness);
    $iz = normalize($indegree);
    $oz = normalize($outdegree);
    $az = normalize($alldegree);
    $icz = normalize($inCloseness);
    $ocz = normalize($outCloseness);



    $tmp = array(0,0,0,0,0,0,0,0);
    for($j=0 ; $j<count($alldegree) ; $j++)
    {
        if(current($pz) > $GLOBAL_Z && !in_array(key($positive_power), $coreElements))
        {
            array_push($coreElements, key($positive_power));
        }
        if(current($nz) > $GLOBAL_Z && !in_array(key($negative_power), $coreElements))
        {
            array_push($coreElements, key($negative_power));
        }
        if(current($bz) > $GLOBAL_Z && !in_array(key($betweenness), $coreElements))
        {
            array_push($coreElements, key($betweenness));
        }
        if(current($icz) > $GLOBAL_Z && !in_array(key($inCloseness), $coreElements))
        {
            array_push($coreElements, key($inCloseness));
        }
        if(current($ocz) > $GLOBAL_Z && !in_array(key($outCloseness), $coreElements))
        {
                  array_push($coreElements, key($outCloseness));
        }
        if(current($iz) > $GLOBAL_Z && !in_array(key($indegree), $coreElements))
        {
            array_push($coreElements, key($indegree));
        }
        if(current($oz) > $GLOBAL_Z && !in_array(key($outdegree), $coreElements))
        {
            array_push($coreElements, key($outdegree));
        }
        if(current($az) > $GLOBAL_Z && !in_array(key($alldegree), $coreElements))
        {
            array_push($coreElements, key($alldegree));
        }

        next($positive_power);
        next($negative_power);
        next($betweenness);
        next($indegree);
        next($outdegree);
        next($alldegree);
        next($inCloseness);
        next($outCloseness);

        next($pz);
        next($nz);
        next($bz);
        next($iz);
        next($oz);
        next($az);
        next($icz);
        next($ocz);

        if(current($pz) < $GLOBAL_Z) $tmp[0] = 1;
        if(current($nz) < $GLOBAL_Z) $tmp[1] = 1;
        if(current($bz) < $GLOBAL_Z) $tmp[2] = 1;
        if(current($iz) < $GLOBAL_Z) $tmp[3] = 1;
        if(current($oz) < $GLOBAL_Z) $tmp[4] = 1;
        if(current($az) < $GLOBAL_Z) $tmp[5] = 1;
        if(current($icz) < $GLOBAL_Z) $tmp[6] = 1;
        if(current($ocz) < $GLOBAL_Z) $tmp[7] = 1;
        if(array_sum($tmp) == count($tmp)) break;

    }

    return $coreElements;
}

function findRelations($startYear, $endYear, $unit, $level, $isNodeExist=array())
{

    global $component;
    $relations = array();

    $query = causeQuery($startYear, $endYear, $unit);
    $result = mysql_query($query);

    $num = 0;
    $code = array();
    $fId = 0;
    $tId = 0;

    $Vertices = array();
    $Arcs = array();
    $partition = array();
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {

        switch($level)
        {
            case 1:
                $fCode = '['.$component[$row['fromComponent']].']'.$row['fromComponent'];
                $tCode = '['.$component[$row['toComponent']].']'.$row['toComponent'];
                break;
            case 2:
                $fCode = '['.$component[$row['fromComponent']].']'.$row['fromAxialCoding'];
                $tCode = '['.$component[$row['toComponent']].']'.$row['toAxialCoding'];
                break;
        }

        if(count($isNodeExist) > 0 && (!in_array($fCode,$isNodeExist) || !in_array($tCode,$isNodeExist)))
        {
            continue;
        }

        if(!is_array($relations[$fCode]))
        {
            $relations[$fCode] = array();
        }
        $relations[$fCode][$tCode]++;
    }

    return $relations;
}
?>