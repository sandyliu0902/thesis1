<?php
require_once 'config.php';
require_once 'excel_writer.php';
require_once 'menu.php';

function excelWrite($filename, $query)
{
    $filename = mb_convert_encoding($filename,'Big5','UTF-8');
    
    if(!is_file($filename))
    {
        touch($filename);
    }
    
    $obj = new ExcelWriter($filename, 'code');
    
    $obj->writeRow();
                    
//    $obj->writeCol('ID');
//    $obj->writeCol('元件');
    $obj->writeCol('元件主軸性編碼');
    $obj->writeCol('年');
//    $obj->writeCol('對象');
//    $obj->writeCol('對象主軸性編碼');
    $obj->writeCol('開放性編碼');
    $obj->writeCol('來源');

    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) 
    {
        $obj->writeRow();
    
//        $obj->writeCol($row['id']);
 //        $obj->writeCol($row['component']);
        $obj->writeCol($row['axialCodingForComponent']);
       $obj->writeCol($row['year']);
///        $obj->writeCol($row['receiver']);
//        $obj->writeCol($row['axialCodingForReceiver']);
        $obj->writeCol($row['openCoding']);
        $obj->writeCol($row['source']);
    }

    
    $obj->close();
 
    mysql_free_result($result);
}

$excelFiles = array(
    array(
        'filename' => 'E:\\論文\\coding\\主軸編碼_價主.xls',
        'query' => 'SELECT * FROM codes WHERE component like "%價主%" ORDER BY axialCodingForComponent DESC, receiver ASC, year ASC ;'),
    array(
        'filename' => 'E:\\論文\\coding\\主軸編碼_產服.xls',
        'query' => 'SELECT * FROM codes WHERE component like "%產服%" ORDER BY axialCodingForComponent DESC, receiver ASC, year ASC ;'),
    array(
        'filename' => 'E:\\論文\\coding\\主軸編碼_資署.xls',
        'query' => 'SELECT * FROM codes WHERE component like "%資署%" ORDER BY axialCodingForComponent DESC, receiver ASC, year ASC ;'),
    array(
        'filename' => 'E:\\論文\\coding\\主軸編碼_組設.xls',
        'query' => 'SELECT * FROM codes WHERE component like "%組設%" ORDER BY axialCodingForComponent DESC, receiver ASC, year ASC ;'),
    array(
        'filename' => 'E:\\論文\\coding\\主軸編碼_價網.xls',
        'query' => 'SELECT * FROM codes WHERE component like "%價網%" ORDER BY axialCodingForComponent DESC, receiver ASC, year ASC ;'),
    array(
        'filename' => 'E:\\論文\\coding\\主軸編碼_財潛.xls',
        'query' => 'SELECT * FROM codes WHERE component like "%財潛%" ORDER BY axialCodingForComponent DESC, receiver ASC, year ASC ;'),
    array(
        'filename' => 'E:\\論文\\coding\\主軸編碼_營機.xls',
        'query' => 'SELECT * FROM codes WHERE component like "%營機%" ORDER BY axialCodingForComponent DESC, receiver ASC, year ASC ;'),
    array(
        'filename' => 'E:\\論文\\coding\\主軸編碼_核策.xls',
        'query' => 'SELECT * FROM codes WHERE component like "%核策%" ORDER BY axialCodingForComponent DESC, receiver ASC, year ASC ;'),
    array(
        'filename' => 'E:\\論文\\coding\\主軸編碼_科技.xls',
        'query' => 'SELECT * FROM codes WHERE component like "%科技%" ORDER BY axialCodingForComponent DESC, receiver ASC, year ASC ;'),
    array(
        'filename' => 'E:\\論文\\coding\\主軸編碼_外部性.xls',
        'query' => 'SELECT * FROM codes WHERE component like "%外部性%" ORDER BY axialCodingForComponent DESC, receiver ASC, year ASC ;'),

);

foreach($excelFiles as $data)
{
    excelWrite($data['filename'], $data['query']);
}

mysql_close($link);

?>
