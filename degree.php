<?php
require_once 'config.php';
require_once 'menu.php';

$query = "SELECT min(year) as minYear, max(year) as maxYear FROM codes WHERE year != '0000'";
$result = mysql_query($query);
$row = mysql_fetch_array($result, MYSQL_ASSOC);
$minYear = $row['minYear'];
$maxYear = $row['maxYear'];
mysql_free_result($result);


/*
 *  Parameter Setting
 */ 
 
$startYear = isset($_GET['startYear']) ? $_GET['startYear'] : $minYear; // 統計起始年
$endYear = isset($_GET['endYear']) ? $_GET['endYear'] : $maxYear; // 統計結束年
$interval = isset($_GET['interval']) ? $_GET['interval'] : 1; // 每幾年唯一個時期
$isAccumulated = isset($_GET['isAccumulated']) && $_GET['isAccumulated'] == 'N' ? false : true; // 是否累計
$order = isset($_GET['order']) ? $_GET['order'] : 'in'; // 是否累計


function display($startYear, $endYear)
{
    global $iterative;
    global $beta;
    global $order;
    global $GLOBAL_Z;
    
    $degree = degree($startYear, $endYear, 1, 1);

    switch($order)
    {
        case 'in':
            arsort($degree['in']);
            break;
        case 'out':
            arsort($degree['out']);
            break;
        case 'all':
            arsort($degree['all']);
            break;
    }    
    
    $z['out'] = normalize($degree['out']);
    $z['in'] = normalize($degree['in']);
    $z['all'] = normalize($degree['all']);
    
    echo '<table width="800" border=0 cellspacing="1" cellpadding="5" style="border-left:1px solid #CCCCCC;border-top:1px solid #CCCCCC;" align="center">';
    echo '<tr><td colspan="7" align="center" style="color:white;background:#333333;"><b>'.$startYear.' ~ '.$endYear.'</b></td></tr>';
    echo '<tr><td width="40%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
              <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">indegree</td>
              <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
              <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">outdegree</td>
              <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
              <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">alldegree</td>
              <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td></tr>';

    foreach($degree[$order] as $code => $value)
    {
        echo '<tr>';
        echo '<td style="color:'.($z[$order][$code] >= $GLOBAL_Z ? '#000000' : '#999999').';">'.$code.'</td>
              <td style="color:'.($z['in'][$code] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($degree['in'][$code] ,0,'.','').'</td>
              <td style="color:'.($z['in'][$code] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($z['in'][$code] ,3,'.','').'</td>
              <td style="color:'.($z['out'][$code] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($degree['out'][$code] ,0,'.','').'</td>
              <td style="color:'.($z['out'][$code] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($z['out'][$code] ,3,'.','').'</td>
              <td style="color:'.($z['all'][$code] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($degree['all'][$code] ,0,'.','').'</td>
              <td style="color:'.($z['all'][$code] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($z['all'][$code] ,3,'.','').'</td></tr>';
        echo '</tr>';
    }
    echo '</table>';
}



?>
<br />
<center>
<form method="GET" id="f">
    時間:
        <select name="startYear" style="width:70px" onChange="document.getElementById('f').submit();">
            <?php 
                for($year=$minYear;$year<=$maxYear;$year++)
                {
                    if($year == $startYear)
                        echo '<option selected="selected" value="'.$year.'">'.$year.'</option>';
                    else
                        echo '<option value="'.$year.'">'.$year.'</option>';
                }
            ?>
        </select>
    年~
        <select name="endYear" style="width:70px" onChange="document.getElementById('f').submit();">
            <?php 
                for($year=$minYear;$year<=$maxYear;$year++)
                {
                    if($year == $endYear)
                        echo '<option selected="selected" value="'.$year.'">'.$year.'</option>';
                    else
                        echo '<option value="'.$year.'">'.$year.'</option>';
                }
            ?>
        </select>
    年，        
    間隔:
        <select name="interval" style="width:50px" onChange="document.getElementById('f').submit();">
            <?php 
                for($i=1;$i<=($maxYear-$minYear+2)/2;$i++)
                {
                    if($i == $interval)
                        echo '<option selected="selected" value="'.$i.'">'.$i.'</option>';
                    else
                        echo '<option value="'.$i.'">'.$i.'</option>';
                }
            ?>
        </select>
    年
    ，是否累計:
        <select name="isAccumulated" style="width:50px" onChange="document.getElementById('f').submit();">
            <option value="Y" <?php echo $isAccumulated ? 'selected="selected"':''; ?>>是</option>
            <option value="N" <?php echo !$isAccumulated ? 'selected="selected"':''; ?>>否</option>
        </select>
    ，排序:
        <select name="order" style="width:70px" onChange="document.getElementById('f').submit();">
            <option value="in" <?php echo ($order == 'in') ? 'selected="selected"':''; ?>>indegree</option>
            <option value="out" <?php echo ($order == 'out') ? 'selected="selected"':''; ?>>outdegree</option>
            <option value="all" <?php echo ($order == 'all') ? 'selected="selected"':''; ?>>alldegree</option>
        </select>
</form>
</center>
<?php

for($year=$startYear ; $year<=$endYear ; $year+=$interval)
{
    if($isAccumulated)
    {
        $currentStartYear = $startYear;
    }
    else
    {
        $currentStartYear = $year;
    }
    
    $currentEndYear = ($year + $interval-1 > $endYear ? $endYear : $year + $interval-1 );
    
    display($currentStartYear, $currentEndYear);    
}


mysql_close($link);

?>
