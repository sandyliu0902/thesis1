<?php
require_once 'config.php';

$mainStartYear = isset($_GET['msy']) ? $_GET['msy'] : false;
$mainEndYear = isset($_GET['mey']) ? $_GET['mey'] : false;

$compareStartYear = isset($_GET['csy']) ? $_GET['csy'] : false;
$compareEndYear = isset($_GET['cey']) ? $_GET['cey'] : false;

$unit = isset($_GET['unit']) ? $_GET['unit'] : false;
$level = isset($_GET['level']) ? $_GET['level'] : false;

if(!($mainStartYear && $mainEndYear && $compareStartYear && $compareEndYear && $unit && $level))
{
    exit;
}

if($level == 1)
{
    foreach($component as $name => $key)
    {
        $allElements[$key-1] = '['.$key.']'.$name; 
    }
    $mainElements = $allElements;
    $compareElements = $allElements;
}
else
{
    $mainElements = findCoreElements($mainStartYear, $mainEndYear, $unit, $level);
    $compareElements = findCoreElements($compareStartYear, $compareEndYear, $unit, $level);
    $allElements = array_merge($mainElements, array_diff($compareElements, $mainElements)  );
    sort($allElements);
}

$mainRelations = findRelations($mainStartYear, $mainEndYear, $unit, $level, $mainElements);
$compareRelations = findRelations($compareStartYear, $compareEndYear, $unit, $level, $compareElements);


function drawLine($im, $fromNode, $toNode, $degree, $radius1, $radius2, $lineColor)
{
    global $allElementsXY;
    
    imageline($im,
              $allElementsXY[$fromNode]['x'], $allElementsXY[$fromNode]['y'],      
              $allElementsXY[$toNode]['x'],$allElementsXY[$toNode]['y'],
              $lineColor);
        
    if(false)
    {
        imagettftext($im, 12, 0, 
                     ($allElementsXY[$fromNode]['x']+$allElementsXY[$toNode]['x'])/2, 
                     ($allElementsXY[$fromNode]['y']+$allElementsXY[$toNode]['y'])/2,
                     $fontColor, "fonts\arial.ttf",  $degree);          
    }
        
    $minusX = $allElementsXY[$toNode]['x'] - $allElementsXY[$fromNode]['x'];
    $minusY = $allElementsXY[$toNode]['y'] - $allElementsXY[$fromNode]['y'];

    if($minusX == 0)
    {
        if($minusY > 0)
        {
            $plus = M_PI_2;
        }
        else
        {
            $plus = M_PI_2 * 3;
        }
    } 
    elseif($minusY == 0)
    {
        if($minusX > 0)
        {
            $plus = 0;
        }
        else
        {
            $plus = M_PI;
        }
    }
    else
    {
        $plus = atan2($minusY,$minusX);
    }
    $minusW = ($radius1 + $radius2 )* cos($plus);
    $minusH = ($radius1 + $radius2 ) * sin($plus);            
     
       
        
    $value = array(
      0  => $allElementsXY[$toNode]['x'] - $minusW  + $radius2 * cos(deg2rad(0)+ $plus),    // x1
      1  => $allElementsXY[$toNode]['y'] - $minusH  + $radius2 * sin(deg2rad(0) + $plus),    // y1
      2  => $allElementsXY[$toNode]['x'] - $minusW  + $radius2 * cos(deg2rad(120) + $plus),    // x2
      3  => $allElementsXY[$toNode]['y'] - $minusH  + $radius2 * sin(deg2rad(120) + $plus),   // y2
      4  => $allElementsXY[$toNode]['x'] - $minusW  + $radius2 * cos(deg2rad(240) + $plus),    // x3
      5  => $allElementsXY[$toNode]['y'] - $minusH  + $radius2 * sin(deg2rad(240) + $plus),    // y3
    );
    imagefilledpolygon($im, $value, 3, $lineColor);
}


if(count($allElements) <= 4)
{
    $imageWidth = 100;
    $imageHeight = 100;
    $margin = 40;
}
elseif(count($allElements) <= 5)
{
    $imageWidth = 150;
    $imageHeight = 150;
    $margin = 40;
}
elseif(count($allElements) <= 7)
{
    $imageWidth = 200;
    $imageHeight = 200;
    $margin = 40;
}
elseif(count($allElements) <= 10)
{
    $imageWidth = 250;
    $imageHeight = 250;
    $margin = 40;
}
elseif(count($allElements) <= 14)
{
    $imageWidth = 300;
    $imageHeight = 300;
    $margin = 40;
}
elseif(count($allElements) <= 19)
{
    $imageWidth = 350;
    $imageHeight = 350;
    $margin = 40;
}
else
{
    $imageWidth = 400;
    $imageHeight = 400;
    $margin = 40;
}



$im = @imagecreatetruecolor(($imageWidth+$margin) * 2 + 220, ($imageHeight+$margin)* 2 )  or die("無法建立圖片！");
        
$bgColor = imagecolorallocate($im, 240,240,240);
$borderColor = imagecolorallocate($im, 50,50,50);

$deleteNodeColor = imagecolorallocate($im, 150,150,150);
$consistNodeColor = imagecolorallocate($im, 0,0,100);
$newNodeColor = imagecolorallocate($im, 100,0,0);

$deleteLineColor = imagecolorallocate($im, 150,150,150);
$consistLineColor = imagecolorallocate($im, 100,100,200);
$newLineColor = imagecolorallocate($im, 200,100,100);

imagefill($im,0,0,$bgColor);


$radius1 = 7; // 圓半徑
$radius2 = 5;  // 箭頭半徑

$allElementsXY = array();

for($i=0 ; $i<count($allElements) ; $i++ )
{
    $allElementsXY[$allElements[$i]]['x'] = $margin + $imageWidth * (1 + cos(deg2rad(360 * $i / count($allElements))));
    $allElementsXY[$allElements[$i]]['y'] = $margin + $imageHeight * (1 + sin(deg2rad(360 * $i / count($allElements)))); 
}

$deleteLineNum = 0;
$consistLineNum = 0;
$newLineNum = 0;
foreach( $mainRelations as $fromNode => $toSets)
{
    foreach($toSets as $toNode => $degree)
    {
        if(isset($compareRelations[$fromNode][$toNode]))
        {
            drawLine($im, $fromNode, $toNode, $degree, $radius1, $radius2, $consistLineColor);
            $consistLineNum++;
        }
        else
        {
            drawLine($im, $fromNode, $toNode, $degree, $radius1, $radius2, $newLineColor);
            $newLineNum++;
        }
        
    }
}
       
foreach( $compareRelations as $fromNode => $toSets)
{
    foreach($toSets as $toNode => $degree)
    {
        if(!isset($mainRelations[$fromNode][$toNode]))
        {
            drawLine($im, $fromNode, $toNode, $degree, $radius1, $radius2, $deleteLineColor);
            $deleteLineNum++;
        }        
    }
}

$deleteNodeNum = 0;
$consistNodeNum = 0;
$newNodeNum = 0;
foreach($allElementsXY as $node => $XY)
{
    if(in_array($node, $mainElements) && in_array($node, $compareElements))
    {
        $nodeColor = $consistNodeColor;
        $lineColor = $consistLineColor;
        $consistNodeNum++;
    }
    elseif(!in_array($node, $mainElements) && in_array($node, $compareElements))
    {
        $nodeColor = $deleteNodeColor;
        $lineColor = $deleteLineColor;
        $deleteNodeNum++;
    }
    elseif(in_array($node, $mainElements) && !in_array($node, $compareElements))
    {
        $nodeColor = $newNodeColor;
        $lineColor = $newLineColor;
        $newNodeNum++;
    }

    imagefilledellipse($im, $XY['x'], $XY['y'], $radius1 * 2, $radius1 * 2, $nodeColor);
    imageellipse($im, $XY['x'], $XY['y'], $radius1 * 2, $radius1 * 2, $lineColor);
    imagettftext($im, 12, 0, $XY['x']+$radius1, $XY['y']+$radius1*2, $nodeColor, "fonts\mingliu.ttc",  $node);          
}


imagettftext($im, 10, 0, $imageWidth * 2 + 170, $imageHeight * 2 - 50 , $newNodeColor, "fonts\arial.ttf", 
             '■ new lines: '.$newLineNum);          
imagettftext($im, 10, 0, $imageWidth * 2 + 170, $imageHeight * 2 - 30 , $consistNodeColor, "fonts\arial.ttf", 
             '■ consist lines: '.$consistLineNum);          
imagettftext($im, 10, 0, $imageWidth * 2 + 170, $imageHeight * 2 - 10 , $deleteNodeColor, "fonts\arial.ttf", 
             '■ delete lines: '.$deleteLineNum);          
imagettftext($im, 10, 0, $imageWidth * 2 + 170, $imageHeight * 2 + 20 , $newNodeColor, "fonts\arial.ttf", 
             '■ new nodes: '.$newNodeNum);          
imagettftext($im, 10, 0, $imageWidth * 2 + 170, $imageHeight * 2 + 40 , $consistNodeColor, "fonts\arial.ttf", 
             '■ consist nodes: '.$consistNodeNum);          
imagettftext($im, 10, 0, $imageWidth * 2 + 170, $imageHeight * 2 + 60 , $deleteNodeColor, "fonts\arial.ttf", 
             '■ delete nodes: '.$deleteNodeNum);          


header("Content-type:image/png");
imagepng($im);
imagedestroy($im);


mysql_close($link);