<?php
require_once 'config.php';
require_once 'menu.php';

function import($contents, $source)
{
    //preg_match_all( '|([FT]?)_?([\d\?]{4})_([^_]*)_([^_]*)_([^_]*)_([^_]*)_([^\n]*)|', $contents, $codes);
    preg_match_all('|([FT]?)_?([\d\?]{4})_([^_]*)\n|', $contents, $codes);

    $result = mysql_query('SELECT max(id) as num FROM codes;');
    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    $currentID = $row['num'] + 1;


    $previousNodeType = '';
    $fromIDs = array();

    foreach ($codes[0] as $key => $code) {
        $codes[3][$key] = preg_replace('|_+|', '_', $codes[3][$key]);

        if ($codes[1][$key] !== 'F' && $codes[1][$key] !== 'T') {
            continue;
        }

        $data = split('_', $codes[3][$key]);

//                  axialCodingForReceiver = "'.htmlspecialchars(trim(array_pop($data))).'",
//                  receiver = "'.htmlspecialchars(trim(array_pop($data))).'",

        $query = 'INSERT INTO codes SET
                  id = ' . $currentID . ',
                  year = ' . (is_numeric($codes[2][$key]) ? $codes[2][$key] : '0000') . ',
                  component = "' . htmlspecialchars(trim(array_shift($data))) . '",
                  openCoding = "' . htmlspecialchars(trim(array_pop($data))) . '",
                  axialCodingForComponent1 = "' . htmlspecialchars(trim(count($data) ? array_shift($data) : '')) . '",
                  axialCodingForComponent2 = "' . htmlspecialchars(trim(count($data) ? array_shift($data) : '')) . '",
                  axialCodingForComponent3 = "' . htmlspecialchars(trim(count($data) ? array_shift($data) : '')) . '",
                  axialCodingForComponent4 = "' . htmlspecialchars(trim(count($data) ? array_shift($data) : '')) . '",
                  source = "' . $source . '";';

        if (!mysql_query($query)) {
            echo $query;
            exit;
        }

        if ($previousNodeType === 'T' && $codes[1][$key] !== 'T') {
            $fromIDs = array();
        }

        if ($codes[1][$key] === 'F') {
            array_push($fromIDs, $currentID);
        } elseif ($codes[1][$key] === 'T') {
            foreach ($fromIDs as $fromID) {
                $query = 'INSERT INTO cause SET
                          fromID = ' . $fromID . ',
                          toID = ' . $currentID . ';';

                mysql_query($query);
            }
        }

        $previousNodeType = $codes[1][$key];

        $currentID++;

    }


    mysql_free_result($result);
}


mysql_query('TRUNCATE TABLE `codes`');
mysql_query('TRUNCATE TABLE `cause`');


$dh = opendir($source_dir);


//*

while (($file = readdir($dh)) !== false) {

    $path = pathinfo($file);
    if (isset($path['extension']) && $path['extension'] === 'txt' && strpos($path['filename'], '~') !== 0) {

        $txt_file = $source_dir . $path['filename'] . '.txt';
        $handle = fopen($txt_file, 'rb');
        import(fread($handle, filesize($txt_file)), $path['filename']);
        fclose($handle);
    }
}

//*/
/*

while (($file = readdir($dh)) !== false) {

    $path = pathinfo( $file);

    if($path['extension'] == 'l' && substr($path['filename'],0,1) != '~')
    {    
        $txt_file = $source_dir.$path['filename']. '.l';
        $handle = fopen($txt_file, "r");
        import( fread($handle, filesize($txt_file)), $path['filename']);
        fclose($handle);
      }
}

//*/


closedir($dh);
mysql_close($link);

echo '<center>Complete!</center>';
