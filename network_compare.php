<?php
require_once 'config.php';
require_once 'menu.php';

$unit = 3;
$level = 1;
$isCompared = true;
 
echo '<center>';
foreach($GLOBAL_PERIODS as $key => $period)
{
    if($key == 0 || !$isCompared)
    {
        echo $period['start'].'-'.$period['end'];
        echo '<br />';
        echo '<br />';
        echo '<img src="network_graph.php?msy='.$period['start'].'&mey='.$period['end'].
             '&csy='.$period['start'].'&cey='.$period['end'].
             '&unit='.$unit.'&level='.$level.'" />';    
        echo '<br />';
        echo '<br />';
        echo '<br />';
    }
    else
    {
        echo $period['start'].'-'.$period['end'];
        echo '<br />';
        echo '<br />';
        echo '<img src="network_graph.php?msy='.$period['start'].'&mey='.$period['end'].
             '&csy='.$GLOBAL_PERIODS[$key-1]['start'].'&cey='.$GLOBAL_PERIODS[$key-1]['end'].
             '&unit='.$unit.'&level='.$level.'" />';
        echo '<br />';
        echo '<br />';
        echo '<br />';
    }
}

echo '<center>';

mysql_close($link);
?>
